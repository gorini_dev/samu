package com.samu;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
public class DatabaseConfig {

	@PersistenceContext
	EntityManager entityManager;
	
    @EventListener
    public void addUnaccent(ContextRefreshedEvent event) { 	
    	try {
    		entityManager.createNativeQuery("CREATE EXTENSION unaccent;").getSingleResult();
		} catch (Exception e) {
			System.out.println(e);
		}
    }
	
}
//