package com.samu;

import java.math.BigInteger;
import java.util.Date;

import com.samu.api.domain.Endereco;
import com.samu.api.domain.IdentificacaoPaciente;
import com.samu.api.domain.Motivo;
import com.samu.api.domain.Pessoa;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
@AllArgsConstructor
public class FichaAtendimentoDTO {

	private String id;
	private IdentificacaoPaciente identificacaoPaciente;
	private Pessoa responsavel;
	private Motivo motivo;
	private Endereco endereco;
	private Date data;
	private BigInteger numeroOcorrencia;
	private String identificacaoUnidade;
	private String url;
	private Boolean enviada;
}


