package com.samu.api;


import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.samu.api.domain.FichaAtendimento;
import com.samu.base.DefaultJpaRepository;

public interface FichaAtendimentoRepository extends DefaultJpaRepository<FichaAtendimento>{
	
	@Transactional
	@Modifying
	@Query("update FichaAtendimento e set e.deleted=true where e.id=:id")
	void softDelete(@Param("id") String id); 
	
	@Query("select f from FichaAtendimento f where"
			+ " LOWER(f.responsavel.nome) like %:query% or "
			+ " LOWER(f.identificacaoPaciente.contato.nome) like %:query% or "
			+ " LOWER(f.identificacaoPaciente.paciente.nome) like %:query% or "
			+ " LOWER(f.motivo.motivo) like %:query% or "
			+ " LOWER(f.motivo.historia) like %:query% or "
			+ " LOWER(cast(f.numeroOcorrencia as text)) like %:query% or "
			+ " LOWER(f.id) like %:query% or "
			+ " LOWER(f.identificacaoUnidade) like %:query% or "
			+ " LOWER(f.endereco.municipio) like %:query% or "
			+ " LOWER(f.endereco.rua) like %:query% or "
			+ " LOWER(f.endereco.bairro) like %:query% or "
			+ " LOWER(to_char(f.data, 'DD/MM/YYYY')) like %:query%")
	Page<FichaAtendimento> search(@Param("query") String query, Pageable page);
	
}
