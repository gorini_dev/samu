package com.samu.api;

import java.text.Normalizer;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.samu.FichaAtendimentoDTO;
import com.samu.api.domain.FichaAtendimento;
import com.samu.api.domain.Hipotese;
import com.samu.api.domain.SinaisVitaisParametrosEvolutivos;
import com.samu.base.PagedResult;
import com.samu.base.RestControllerCORS;
import com.samu.spring.utils.DeepCopy;

import io.swagger.annotations.Api;

@RestControllerCORS
@Api(tags = "Ficha de Atendimento")
@RequestMapping("/ficha")
public class FinchaAtendimentoController {

	@Autowired
	FichaAtendimentoRepository repo;
	
	@Autowired
	DeepCopy copy;
	
	@GetMapping("/{id}")
	public ResponseEntity<FichaAtendimento> obterPorId(@PathVariable String id){
		Optional<FichaAtendimento> opt = repo.findById(id);
		if(opt.isPresent()) {
			opt.get().setUrl(getUrlLink(opt.get().getId()));
			return ResponseEntity.ok(opt.get());			
		}
		return ResponseEntity.notFound().build();
	}
	
	@GetMapping("/search")
	public ResponseEntity<PagedResult<FichaAtendimentoDTO>> obterFichaPaginada (
			@RequestParam(value="page", required=false, defaultValue="0") Integer page,
			@RequestParam(value="limit", required=false, defaultValue="10") Integer limit,
			@RequestParam(value="search", required=false, defaultValue="") String search
			){
		
		final PageRequest paginacao = PageRequest.of(page, limit, Direction.DESC, "updatedAt");
		Page<FichaAtendimento> fichaPaginada = repo.search(removeDiacritics(search.toLowerCase()), paginacao);
		
		
		Integer lastPage = fichaPaginada.getTotalPages()-1;
		lastPage = lastPage == -1 ? 0: lastPage;
		
		PagedResult<FichaAtendimentoDTO> resumoFichaPaginada = new PagedResult<>(
			fichaPaginada.getContent().stream().map(x -> new FichaAtendimentoDTO(
					x.getId(), 
					x.getIdentificacaoPaciente(),
					x.getResponsavel(),
					x.getMotivo(), 
					x.getEndereco(), 
					x.getData(), 
					x.getNumeroOcorrencia(), 
					x.getIdentificacaoUnidade(), 
					getUrlLink(x.getId()),true)).collect(Collectors.toList()),
			limit,
			page,
			lastPage
		);
		
		return ResponseEntity.ok(resumoFichaPaginada);
		
	}
	
	//@RoleUser
	@PostMapping
	public ResponseEntity<FichaAtendimento> novo(@RequestBody FichaAtendimento ficha){
		FichaAtendimento fichaA = repo.save(ficha);
		fichaA.setUrl(fichaA.getId());
		return ResponseEntity.ok(fichaA);
	}
	
	//@RoleUser
	@PutMapping("/{id}")
	public ResponseEntity<FichaAtendimento> alterar(@PathVariable String id, @RequestBody FichaAtendimento ficha){
		Optional<FichaAtendimento> opt = repo.findById(id);
		if(opt.isPresent()){
			FichaAtendimento fichaAntiga = opt.get();
			copy.copyFields(ficha, fichaAntiga, "id");
			fichaAntiga.getHipotesesDiagnosticas().getHipoteses().clear();
			
			for (Hipotese hipotese : fichaAntiga.getHipotesesDiagnosticas().getHipoteses()) {
				fichaAntiga.getHipotesesDiagnosticas().getHipoteses().add(hipotese);
			}
			
			fichaAntiga.getSinaisVitaisParametrosEvolutivos().getMedicoes().clear();
			for (SinaisVitaisParametrosEvolutivos sinaisVitaisParametrosEvolutivos : ficha.getSinaisVitaisParametrosEvolutivos().getMedicoes()) {
				fichaAntiga.getSinaisVitaisParametrosEvolutivos().getMedicoes().add(sinaisVitaisParametrosEvolutivos);
			}
			FichaAtendimento fichaA = repo.save(fichaAntiga);
			fichaA.setUrl(fichaA.getId());
			return ResponseEntity.ok(fichaA);
		}
		return ResponseEntity.notFound().build();
	}
	
	//@RoleUser
	@DeleteMapping("/{id}")
	public ResponseEntity<Void> deletar(@PathVariable String id){
		if(repo.existsById(id)){
			repo.softDelete(id);
			return ResponseEntity.ok().build();
		}
		return ResponseEntity.notFound().build();
	}

	private static String removeDiacritics(String value) {
	    return Normalizer.normalize(value, Normalizer.Form.NFD).replaceAll("[^\\p{ASCII}]", "");
	}
	
	private String getUrlLink(String id) {
		return ServletUriComponentsBuilder.fromCurrentContextPath()
        .path("/ficha.html")
        .query("id="+id)
        .toUriString();
	}
	
}
