package com.samu.api.domain;

import javax.persistence.Entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.samu.base.DefaultEntity;

import lombok.Getter;
import lombok.NoArgsConstructor;

@Entity
@Getter
@NoArgsConstructor
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Circulacao extends DefaultEntity{
	private Boolean paradaCardioRespiratorio;
	private String pulso;
	private String palidezPele;
	private String temperaturaPele;
	private String umidadePele;
	private String dorToracica;
	private String pressaoArterial;
	private String choque;
	private String edema;
}
