package com.samu.api.domain;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.samu.base.DefaultEntity;

import lombok.Getter;
import lombok.NoArgsConstructor;

@Entity
@Getter
@NoArgsConstructor
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Destino extends DefaultEntity{
	private Date dataHora;
	
	@Column(length=999)
	private String observacoes;
	
	@ManyToOne(cascade=CascadeType.ALL) private Endereco endereco;
	@ManyToOne(cascade=CascadeType.ALL) private ProfissionalReceptor profissionalReceptor;
}
