package com.samu.api.domain;

import java.math.BigDecimal;

import javax.persistence.Entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.samu.base.DefaultEntity;

import lombok.Getter;
import lombok.NoArgsConstructor;

@Entity
@Getter
@NoArgsConstructor
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Endereco extends DefaultEntity{
	private String pais;
	private String estado;
	private String municipio;
	private String rua;
	private String numero;
	private String bairro;
	private String referencias;
	private String complemento;
	private String descricao;
	private BigDecimal coordenadaX;
	private BigDecimal coordenadaY;
	private BigDecimal coordenadaMargem;
}

