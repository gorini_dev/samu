package com.samu.api.domain;

import java.math.BigInteger;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import org.hibernate.annotations.Where;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.samu.base.DefaultEntity;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Where(clause="deleted=false")
@Entity
@Getter
@NoArgsConstructor
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class FichaAtendimento extends DefaultEntity {
	
	private Date data;
	private BigInteger numeroOcorrencia;
	private String identificacaoUnidade;
	private BigInteger progresso;
	private String url;
	private Boolean emProgresso;
	private Boolean enviada;
	@JsonIgnore
	private Boolean deleted = false;
	
	@ManyToOne(cascade=CascadeType.ALL, fetch=FetchType.EAGER)	private Pessoa responsavel;
	@ManyToOne(cascade=CascadeType.ALL, fetch=FetchType.EAGER)  private Endereco endereco;
	@ManyToOne(cascade=CascadeType.ALL, fetch=FetchType.EAGER)	private IdentificacaoPaciente identificacaoPaciente;
	@ManyToOne(cascade=CascadeType.ALL, fetch=FetchType.EAGER)	private Motivo motivo;
	@ManyToOne(cascade=CascadeType.ALL, fetch=FetchType.LAZY)	private EstadoAtual estadoAtual;
	@ManyToOne(cascade=CascadeType.ALL, fetch=FetchType.LAZY)	private EstadoInicial estadoInicial;
	@ManyToOne(cascade=CascadeType.ALL, fetch=FetchType.LAZY)	private ViasAereas viasAereas;
	@ManyToOne(cascade=CascadeType.ALL, fetch=FetchType.LAZY)	private ECG ecg;
	@ManyToOne(cascade=CascadeType.ALL, fetch=FetchType.LAZY)	private Respiracao respiracao;
	@ManyToOne(cascade=CascadeType.ALL, fetch=FetchType.LAZY)	private Circulacao circulacao;
	@ManyToOne(cascade=CascadeType.ALL, fetch=FetchType.LAZY)	private Neuro neuro;
	@ManyToOne(cascade=CascadeType.ALL, fetch=FetchType.LAZY)	private Traumas traumas;
	@ManyToOne(cascade=CascadeType.ALL, fetch=FetchType.LAZY)	private Queimaduras queimaduras;
	@ManyToOne(cascade=CascadeType.ALL, fetch=FetchType.LAZY)	private Ginecologia ginecologia;
	@ManyToOne(cascade=CascadeType.ALL, fetch=FetchType.LAZY)	private Abdomen abdomen;
	@ManyToOne(cascade=CascadeType.ALL, fetch=FetchType.LAZY)	private RecusaAtendimento recusaAtendimento;
	@ManyToOne(cascade=CascadeType.ALL, fetch=FetchType.LAZY)	private Destino destino;
	@ManyToOne(cascade=CascadeType.ALL, fetch=FetchType.LAZY)	private Conduta evolucaoConduta;
	@ManyToOne(cascade=CascadeType.ALL, fetch=FetchType.LAZY)	private HipotesesDiagnosticas hipotesesDiagnosticas;
	@ManyToOne(cascade=CascadeType.ALL, fetch=FetchType.LAZY)	private MedicoesSinaisVitaisParametrosEvolutivos sinaisVitaisParametrosEvolutivos;
	
	@Override
	@JsonIgnore(value=false)
	public String getId() {
		return super.getId();
	}
	
	public void setUrl(String url) {
		this.url = url;
	}
	
}
