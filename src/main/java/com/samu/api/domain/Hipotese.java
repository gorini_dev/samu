package com.samu.api.domain;

import java.util.Date;
import javax.persistence.Entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.samu.base.DefaultEntity;

import lombok.Getter;
import lombok.NoArgsConstructor;

@Entity
@Getter
@NoArgsConstructor
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Hipotese extends DefaultEntity{
	private Date data;
	private Integer numero;
	private String cid;
	private String descricao;
}
