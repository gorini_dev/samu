package com.samu.api.domain;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.samu.base.DefaultEntity;

import lombok.Getter;
import lombok.NoArgsConstructor;

@Entity
@Getter
@NoArgsConstructor
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class IdentificacaoPaciente extends DefaultEntity{
	@ManyToOne(cascade=CascadeType.ALL) private Pessoa paciente;
	@ManyToOne(cascade=CascadeType.ALL) private Contato contato;
}
