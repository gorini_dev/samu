package com.samu.api.domain;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.samu.base.DefaultEntity;

import lombok.Getter;
import lombok.NoArgsConstructor;

@Entity
@Getter
@NoArgsConstructor
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class MedicoesSinaisVitaisParametrosEvolutivos extends DefaultEntity{
	
	@OneToMany(cascade=CascadeType.ALL, orphanRemoval=true) 
	@JoinColumn(name="medicoes_sinais_vitais_parametros_evolutivos_id") 
	private List<SinaisVitaisParametrosEvolutivos> medicoes = new ArrayList<>();
	
}
