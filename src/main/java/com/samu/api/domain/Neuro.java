package com.samu.api.domain;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.samu.base.DefaultEntity;

import lombok.Getter;
import lombok.NoArgsConstructor;

@Entity
@Getter
@NoArgsConstructor
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Neuro extends DefaultEntity{
	private Boolean convulsao;
	private Boolean sdMeningea;
	private String consciencia;
	private String aberturaOcular;
	private String respostaVerbal;
	private String respostaMotora;
	private String deficitSensitivo;
	private String deficitMotor; 
	@ManyToOne(cascade=CascadeType.ALL) private Pupilas pupilas;
}