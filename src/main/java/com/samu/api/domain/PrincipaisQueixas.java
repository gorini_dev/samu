package com.samu.api.domain;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.samu.base.DefaultEntity;

import lombok.Getter;
import lombok.NoArgsConstructor;

@Entity
@Getter
@NoArgsConstructor
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class PrincipaisQueixas extends DefaultEntity{
	@ManyToOne(cascade=CascadeType.ALL) private Queixa dificuldadeRespiratoria;
	@ManyToOne(cascade=CascadeType.ALL) private Queixa dor;
	@ManyToOne(cascade=CascadeType.ALL) private Queixa febre;
	@ManyToOne(cascade=CascadeType.ALL) private Queixa nauseas;
	@ManyToOne(cascade=CascadeType.ALL) private Queixa vomitos;
	@ManyToOne(cascade=CascadeType.ALL) private Queixa tonturas;
	@ManyToOne(cascade=CascadeType.ALL) private Queixa alergias;
	@ManyToOne(cascade=CascadeType.ALL) private Queixa outros;
}
