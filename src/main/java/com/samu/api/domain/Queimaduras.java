package com.samu.api.domain;

import javax.persistence.Entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.samu.base.DefaultEntity;

import lombok.Getter;
import lombok.NoArgsConstructor;

@Entity
@Getter
@NoArgsConstructor
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Queimaduras extends DefaultEntity{
	private Integer cabeca;
	private Integer pescoco;
	private Integer tant;
	private Integer tpos;
	private Integer perineo;
	private Integer msd;
	private Integer mse;
	private Integer mid;
	private Integer mie;
}
