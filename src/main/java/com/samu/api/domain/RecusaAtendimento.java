package com.samu.api.domain;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.samu.base.DefaultEntity;

import lombok.Getter;
import lombok.NoArgsConstructor;

@Entity
@Getter
@NoArgsConstructor
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class RecusaAtendimento extends DefaultEntity{
	@Column(length=9999)
	private String assinatura;
	@ManyToOne(cascade=CascadeType.ALL) private Pessoa pessoa;
	
	private Boolean ativo;
}

