package com.samu.api.domain;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.samu.base.DefaultEntity;

import lombok.Getter;
import lombok.NoArgsConstructor;

@Entity
@Getter
@NoArgsConstructor
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Respiracao extends DefaultEntity{
	private Boolean enfisemaSubCutaneo;
	private Boolean hemoptise;
	private String nivelRespiracao;
	private String auxilioRespiracao;
	private String ausculta;
	private String mv;
	private String expansibilidade;
	@ManyToOne(cascade=CascadeType.ALL) private Ritmo ritmoIrregular;
}
