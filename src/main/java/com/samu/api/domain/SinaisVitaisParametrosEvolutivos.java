package com.samu.api.domain;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.samu.base.DefaultEntity;

import lombok.Getter;
import lombok.NoArgsConstructor;

@Entity
@Getter
@NoArgsConstructor
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class SinaisVitaisParametrosEvolutivos extends DefaultEntity{
	private Date data;
	private Integer numero;
	private Integer frequenciaCardiaca;
	private Integer frequenciaRespiratoria;
	private Integer glasgow;
	private BigDecimal temperatura;
	private Integer spo2;
	private Integer hgt;
	@ManyToOne(cascade=CascadeType.ALL) private PresaoArterial presaoArterial;
}
