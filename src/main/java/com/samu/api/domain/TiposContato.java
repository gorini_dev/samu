package com.samu.api.domain;

import javax.persistence.Entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.samu.base.DefaultEntity;

import lombok.Getter;
import lombok.NoArgsConstructor;

@Entity
@Getter
@NoArgsConstructor
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class TiposContato extends DefaultEntity{
	private Boolean amigo;
	private Boolean familia;
	private Boolean testemunha;
	private Boolean medico;
	private Boolean socorrista;
	private Boolean policia;
}
