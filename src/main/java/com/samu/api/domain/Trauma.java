package com.samu.api.domain;

import javax.persistence.Entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.samu.base.DefaultEntity;

import lombok.Getter;
import lombok.NoArgsConstructor;

@Entity
@Getter
@NoArgsConstructor
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Trauma extends DefaultEntity{
	private Boolean escoriacao;
	private Boolean contusao;
	private Boolean ferimento;
	private Boolean luxacao;
	private Boolean fratura;
	private Boolean laceracao;
	private Boolean seccao;
}
