package com.samu.api.domain;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.samu.base.DefaultEntity;

import lombok.Getter;
import lombok.NoArgsConstructor;

@Entity
@Getter
@NoArgsConstructor
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Traumas extends DefaultEntity{
	@ManyToOne(cascade=CascadeType.ALL) private Trauma cranio;
	@ManyToOne(cascade=CascadeType.ALL) private Trauma face;
	@ManyToOne(cascade=CascadeType.ALL) private Trauma coluna;
	@ManyToOne(cascade=CascadeType.ALL) private Trauma torax;
	@ManyToOne(cascade=CascadeType.ALL) private Trauma bacia;
	@ManyToOne(cascade=CascadeType.ALL) private Trauma msd;
	@ManyToOne(cascade=CascadeType.ALL) private Trauma mse;
	@ManyToOne(cascade=CascadeType.ALL) private Trauma mid;
	@ManyToOne(cascade=CascadeType.ALL) private Trauma mie;
}
