package com.samu.api.sender;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class Email {

	private String subject;
	private String content;
	private String from;
	private String to;
	private EmailType type;
	
}
