package com.samu.api.sender;

import java.io.IOException;

import org.springframework.stereotype.Service;

import com.sendgrid.Content;
import com.sendgrid.Email;
import com.sendgrid.Mail;
import com.sendgrid.Method;
import com.sendgrid.Request;
import com.sendgrid.SendGrid;

@Service
public class EmailService{
    public Boolean sendEmailUsingSendGrid(com.samu.api.sender.Email email){
    	Email from = new Email(email.getFrom());
        Email to = new Email(email.getTo());
        Content content = new Content(email.getType().getValue(), email.getContent());
        Mail mail = new Mail(from, email.getSubject(), to, content);

        SendGrid sg = new SendGrid("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");
        Request request = new Request();
        try {
          request.setMethod(Method.POST);
          request.setEndpoint("mail/send");
          request.setBody(mail.build());
          sg.api(request);    
        } catch (IOException ex) {
        	System.out.println(ex.toString());
        	 return false;
        }
        return true;
    }
}