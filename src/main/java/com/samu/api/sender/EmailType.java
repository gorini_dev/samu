package com.samu.api.sender;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum EmailType {
	TEXT("text/plain"), HTML("text/html");
	
	private String value;
	
}

