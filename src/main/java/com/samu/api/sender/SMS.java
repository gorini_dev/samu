package com.samu.api.sender;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@Getter
@NoArgsConstructor
public class SMS {

	private String message;
	private String phoneNumber;
	
	
}
