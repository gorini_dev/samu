package com.samu.api.sender;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import org.springframework.stereotype.Service;

@Service
public class SMSService {
	
	public boolean sendSms(String _apiKey,String _message,String _sender,String _numbers) {
		try {
			// Construct data
			String apiKey = "apikey=" + _apiKey;
			String message = "&message=" + _message;
			String sender = "&sender=" + _sender;
			String numbers = "&numbers=55" + _numbers;
			
			// Send data
			HttpURLConnection conn = (HttpURLConnection) new URL("https://api.txtlocal.com/send/?").openConnection();
			String data = apiKey + numbers + message + sender;
			conn.setDoOutput(true);
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Content-Length", Integer.toString(data.length()));
			conn.getOutputStream().write(data.getBytes("UTF-8"));
			final BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
			final StringBuffer stringBuffer = new StringBuffer();
			String line;
			while ((line = rd.readLine()) != null) {
				stringBuffer.append(line);
			}
			rd.close();
			System.out.println(stringBuffer.toString());
			return true;
		} catch (Exception e) {
			System.out.println("Error SMS "+e);
			return false;
		}
	}
}
