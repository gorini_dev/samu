package com.samu.api.sender;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.samu.base.RestControllerCORS;

import io.swagger.annotations.Api;

@RestControllerCORS
@Api(tags = "Send")
@RequestMapping
public class SenderController {

	@Autowired
	EmailService emailService;
		
	@Autowired
	SMSService smsService;
	
	@PostMapping("/email")
	public ResponseEntity<Object> sendEmail(@RequestBody Email email){
		return ResponseEntity.ok(emailService.sendEmailUsingSendGrid(email));
	}
	
	@PostMapping("/sms")
	public ResponseEntity<Object> sendSMS(@RequestBody SMS sms){
		return ResponseEntity.ok(
				smsService.sendSms(
						"ZCLRWbbdx94-ZBE92eNgWZ8KaE303qinK5PtbxWFCn", 
						sms.getMessage(),"Samu",  sms.getPhoneNumber()));
		
	}
	
}
