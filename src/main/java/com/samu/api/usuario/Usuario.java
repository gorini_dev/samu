package com.samu.api.usuario;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.validation.constraints.NotEmpty;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.samu.base.DefaultEntity;

import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
public class Usuario extends DefaultEntity implements UserDetails{
	private static final long serialVersionUID = 1L;

	@Column(unique=true, nullable=false)
	private String token;
		
	@Column(unique=true, updatable=false)
	private String email;
	
	@NotEmpty
	private String nome;
	
	private String unidadePadrao;
	
	@ElementCollection(fetch = FetchType.EAGER)
	private List<String> roles = new ArrayList<>();
	
	@JsonIgnore private String senha;
	@JsonIgnore private boolean isAccountNonExpired;
	@JsonIgnore private boolean isAccountNonLocked;
	@JsonIgnore private boolean isCredentialsNonExpired;
	@JsonIgnore private boolean isEnabled;	

	@Override
	@JsonIgnore
	public Collection<? extends GrantedAuthority> getAuthorities() {
		List<SimpleGrantedAuthority> authorities = new ArrayList<>();
		for (String role : roles) {
			authorities.add(new SimpleGrantedAuthority(role));
		}
		return authorities;
	}

	@Override
	@JsonIgnore
	public boolean isAccountNonExpired() {
		return isAccountNonExpired;
	}

	@Override
	@JsonIgnore
	public boolean isAccountNonLocked() {
		return isAccountNonLocked;
	}

	@Override
	@JsonIgnore
	public boolean isCredentialsNonExpired() {
		return isCredentialsNonExpired;
	}

	@Override
	@JsonIgnore
	public boolean isEnabled() {
		return isEnabled;
	}

	@Override
	public String getPassword() {
		return senha;
	}

	@Override
	public String getUsername() {
		return email;
	}
	
	
}
