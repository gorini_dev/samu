package com.samu.api.usuario;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.samu.base.RestControllerCORS;
import com.samu.config.auth.RoleUser;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestControllerCORS
@Api(tags = "Usuario")
@RequestMapping("/usuario")
public class UsuarioController {

	@Autowired
	UsuarioService usuarioAccountService;
	
    @PostMapping("/cadastrar")
    public ResponseEntity<Usuario> cadastrar(@RequestBody @Valid UsuarioDTONovo userAccountDTO){
    	return ResponseEntity.ok(usuarioAccountService.novo(userAccountDTO));
    }
    
    @PostMapping("/acessar")
    public ResponseEntity<UsuarioToken> acessar(@RequestBody @Valid UsuarioLogin login){
    	return ResponseEntity.ok(usuarioAccountService.obterToken(login.getEmail(), login.getSenha()));
    }
    
    @RoleUser
    @PutMapping("/senha")
    public ResponseEntity<Usuario> alterar(@RequestBody @Valid UsuarioDTOSenha userDTO){
    	return ResponseEntity.ok(usuarioAccountService.alterar(userDTO));
    }
    
    @RoleUser
    @PutMapping
    public ResponseEntity<Usuario> alterar(@RequestBody @Valid UsuarioDTOAlterar userDTO){
    	return ResponseEntity.ok(usuarioAccountService.alterar(userDTO));
    }
    	
    @RoleUser
    @GetMapping
    public ResponseEntity<Usuario> obter(){
    	Usuario userAccount = (Usuario) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return  ResponseEntity.ok( (Usuario) usuarioAccountService.buscarPorToken(userAccount.getToken()));
    }
    
    @RoleUser
    @PostMapping("/conceder/admin")
    @ApiOperation(notes="access=123456", value = "@RoleUser")
    public ResponseEntity<UsuarioToken> concederAdmin(@RequestParam("access") String access){
    	Usuario userAccount = (Usuario) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    	if(access.equals("123456"))
    		return  ResponseEntity.ok( usuarioAccountService.concederAdmin(userAccount.getToken()));
    	return ResponseEntity.badRequest().build();
    }
     
}
