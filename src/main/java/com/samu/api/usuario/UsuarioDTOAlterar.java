package com.samu.api.usuario;



import javax.validation.constraints.NotEmpty;

import org.springframework.security.core.context.SecurityContextHolder;


import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
public class UsuarioDTOAlterar implements UsuarioDTO {

	@NotEmpty
	private String nome;
	
	private String unidadePadrao;
	
	@JsonIgnore
	private String userAccountId = ((Usuario) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getId();
	
}
