package com.samu.api.usuario;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
public class UsuarioDTONovo implements UsuarioDTO{

	@Email
	private String email;
	
	@NotEmpty
	@Size(min=6)
	private String senha;
			
	@NotEmpty
	private String nome;
	
	private String unidadePadrao;
	
	@Override
	public String getUserAccountId() {
		return null;
	}
}
