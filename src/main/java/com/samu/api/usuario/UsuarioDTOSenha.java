package com.samu.api.usuario;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

import org.springframework.security.core.context.SecurityContextHolder;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
public class UsuarioDTOSenha implements UsuarioDTO{
	
	@NotEmpty
	@Size(min=6)
	private String senha;

	@JsonIgnore
	private String userAccountId = ((Usuario) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getId();
}
