package com.samu.api.usuario;

import javax.validation.constraints.NotEmpty;

import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
public class UsuarioLogin {

	@NotEmpty
    private String email;
	
	@NotEmpty
    private String senha;

}