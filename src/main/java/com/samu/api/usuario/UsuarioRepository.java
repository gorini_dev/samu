package com.samu.api.usuario;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.samu.base.DefaultJpaRepository;

public interface UsuarioRepository extends DefaultJpaRepository<Usuario>{

	Usuario findUsuarioByToken(String token);
	Usuario findUsuarioByEmail(String email);
	Usuario findUsuarioByEmailAndSenha(String email, String senha);
	
	@Query("SELECT u.token FROM Usuario u where u.email = :email and u.senha = :senha")
	String findTokenByEmailAndSenha(@Param("email") String email, @Param("senha") String senha);
	
}
