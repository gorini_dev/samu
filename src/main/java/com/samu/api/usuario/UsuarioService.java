package com.samu.api.usuario;

import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.samu.base.BaseService;
import com.samu.base.PagedResult;
import com.samu.base.mapper.MapperService;
import com.samu.config.auth.RoleSystem;
import com.samu.config.auth.TokenProvider;
import com.samu.exception.CredenciaisInvalidasException;
import com.samu.exception.EntidadeNaoEncontradaPorIdException;


@Service
public class UsuarioService implements BaseService<Usuario, UsuarioDTONovo>{

	@Autowired
	private UsuarioRepository userAccountRepository;
	
	@Autowired
	private MapperService<?> mapper;
	
	@Autowired
	private TokenProvider tokenProvider;
	
	public Usuario buscarPorToken(String token){
		return userAccountRepository.findUsuarioByToken(token);
	}

	public Usuario novo(UsuarioDTONovo userAccountDTO){
		Usuario userAccount = new Usuario();
		mapper.parseDTOToEntity(userAccountDTO, userAccount);
		userAccount.getRoles().add(RoleSystem.ROLE_USER.name());
		userAccount.setToken(tokenProvider.generateNewToken());
		return userAccountRepository.save(userAccount);
	}
	
	public Usuario alterar(UsuarioDTO userAccountDTO) {
		String id = userAccountDTO.getUserAccountId();
		Optional<Usuario> userAccountOptional = userAccountRepository.findById(id);
		if(userAccountOptional.isPresent()) {
			Usuario userAccount = userAccountOptional.get();
			mapper.parseDTOToEntity(userAccountDTO, userAccount);
			userAccount.setToken(tokenProvider.generateNewToken());
			return userAccountRepository.save(userAccount); 
		}
		throw new EntidadeNaoEncontradaPorIdException(id);
	}

	public UsuarioToken obterToken(String email, String senha) {
		String token = userAccountRepository.findTokenByEmailAndSenha(email, senha);
		if(token == null || token.isEmpty())
			throw new CredenciaisInvalidasException();
		return new UsuarioToken(token);
	}	

	public UsuarioToken concederAdmin(String token) {
		Usuario userAccount = userAccountRepository.findUsuarioByToken(token);
		if(userAccount != null) {
			userAccount.setToken(tokenProvider.generateNewToken());
			userAccount.getRoles().add(RoleSystem.ROLE_ADMIN.name());
			return new UsuarioToken(userAccountRepository.save(userAccount).getToken()); 
		}
		throw new CredenciaisInvalidasException();
	}

	@Override
	public Usuario create(UsuarioDTONovo entityDTO) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Usuario findById(String id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Usuario> findByIds(List<?> list) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Usuario change(String id, UsuarioDTONovo entityDTO) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void remove(String id) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<Usuario> findAll() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public PagedResult<Usuario> findAllPaged(Integer page, Integer limit, Boolean desc, Boolean searchBetween,
			String orderField, String search) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void forceDelete(String id) {
		// TODO Auto-generated method stub
		
	}
	
}
