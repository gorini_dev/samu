package com.samu.api.usuario;

public class UsuarioToken {
	private String token;

	public UsuarioToken(String token) {
		this.token = token;
	}

	public String getToken() {
		return token;
	}
	
}
