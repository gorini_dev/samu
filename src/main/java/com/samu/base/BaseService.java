package com.samu.base;

import java.util.List;

public interface BaseService<E, E2> {
	
	E create(final E2 entityDTO);
	E findById(final String id);
	List<E> findByIds(final List<?> list);
	E change(final String id, final E2 entityDTO);
	void remove(final String id);
	List<E> findAll();
	PagedResult<E> findAllPaged(final Integer page, final Integer limit, final Boolean desc, final Boolean searchBetween, final String orderField, final String search );
	void forceDelete(String id);
	
}
