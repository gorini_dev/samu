package com.samu.base;

import java.time.LocalDateTime;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

@MappedSuperclass
public class DefaultEntity {

	@Id
	@JsonProperty(access = Access.READ_ONLY)
	@Column(updatable=false)
	@JsonIgnore
	private String id;
	
	private Boolean preenchido;
	
	@CreationTimestamp
	@Column(updatable=false)
    private LocalDateTime createdAt;
	
	@UpdateTimestamp
    private LocalDateTime  updatedAt;
	
	public DefaultEntity() {
		id = UUID.randomUUID().toString();
	}
	
	public DefaultEntity(String id) {
		this.id = UUID.fromString(id).toString();
	}
	
	@JsonIgnore
	public String getId() {
		return id;
	}

	@JsonIgnore
	public void setId(String id) {
		this.id = id;
	}
	
	public Boolean getPreenchido() {
		return preenchido;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonIgnore
	public LocalDateTime getCreatedAt() {
		return createdAt;
	}

	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonIgnore
	public LocalDateTime  getUpdatedAt() {
		return updatedAt;
	}

}
