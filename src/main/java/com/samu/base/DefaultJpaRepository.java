package com.samu.base;

import org.springframework.data.jpa.repository.JpaRepository;

public interface DefaultJpaRepository <E extends DefaultEntity> extends JpaRepository<E, String> {
	
}