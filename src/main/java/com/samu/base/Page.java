package com.samu.base;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class Page {

	private Integer offset;
	private Integer limit;
	
}
