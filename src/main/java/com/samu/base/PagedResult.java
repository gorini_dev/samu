package com.samu.base;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@Getter
@NoArgsConstructor
@Builder
public class PagedResult<T> {
    
	private List<T> content;
    private Integer limit;
    private Integer page;
    private Integer lastPage;

}