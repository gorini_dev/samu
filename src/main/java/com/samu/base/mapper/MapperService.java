package com.samu.base.mapper;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.samu.base.BaseService;
import com.samu.base.DefaultEntity;
import com.samu.exception.ImpossivelParsearDTOParaEntidadeException;



@Service
public class MapperService<E> {

	private static final String EMPTY = "";
	private static final String SERVICE_SPLIT = "Service";
	private static final String SUFFIX_MULTIPLE_IDS = "Ids";
	private static final String SUFFIX_SINGLE_ID = "Id";
	
	@Autowired
	private List<BaseService<?,?>> services;
		
	@SuppressWarnings("unchecked")
	public E parseDTOToEntity(Object dto, Object entity) {
		try {
			Field[] dtoFields = dto.getClass().getDeclaredFields();
			Field[] entityFields = entity.getClass().getDeclaredFields();
			for (Field dtoField : dtoFields) {
				dtoField.setAccessible(true);
				Object value = null;
				Field entityField = null;
				if(existsField(dtoField,entityFields)) {
					entityField = entity.getClass().getDeclaredField(dtoField.getName());
					value = dtoField.get(dto);
				}else {
					for (BaseService<?,?> service : services) {
						String domainNameClass = (service.getClass().getSimpleName()).split(SERVICE_SPLIT)[0];
						if(domainNameClass.equalsIgnoreCase(removeStringIdOrIds(dtoField))) {
							entityField = entity.getClass().getDeclaredField(dtoField.getDeclaredAnnotation(MapperDTO.class).value());
							Object fieldValue = dtoField.get(dto);
							if(fieldValue != null)
								value = extractValueFromFieldValue(dtoField, service, fieldValue);
							break;
						}
					}
				}
				if(entityField!= null) {
					entityField.setAccessible(true);
					entityField.set(entity, value);
					entityField.setAccessible(false);				
				}
				dtoField.setAccessible(false);
			}
			return (E) entity;
		}catch (Exception e) {
			throw new ImpossivelParsearDTOParaEntidadeException(e);
		}
	}

	private Object extractValueFromFieldValue(Field dtoField, BaseService<?, ?> service, Object fieldValue) {
		return isFieldList(dtoField) ? (service.findByIds((List<?>)fieldValue)).stream().map(x -> service.findById(((DefaultEntity)x).getId())).collect(Collectors.toList()):  service.findById((String)fieldValue);
	}

	private boolean existsField(Field searchField, Field[] fields) {
		return Arrays.stream(fields)
				.anyMatch(field -> field.getName().equals(searchField.getName()));
	}
	
	private boolean isFieldList(Field field) {
		return field.getName().endsWith(SUFFIX_MULTIPLE_IDS);
	}

	private String removeStringIdOrIds(Field field) {
		return isFieldList(field) ? field.getName().replace(SUFFIX_MULTIPLE_IDS, EMPTY): field.getName().replace(SUFFIX_SINGLE_ID, EMPTY);
	}

}
