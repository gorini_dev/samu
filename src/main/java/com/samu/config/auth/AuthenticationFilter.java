package com.samu.config.auth;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.web.filter.OncePerRequestFilter;

import com.samu.api.usuario.UsuarioService;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class AuthenticationFilter extends OncePerRequestFilter {

    @Autowired
    private UsuarioService userAccountService;   
    
    @Override
    protected void doFilterInternal(HttpServletRequest req, HttpServletResponse res, FilterChain chain) throws IOException, ServletException {
    	String token = req.getHeader("Token");  	
    	
    	if(token != null && !token.isEmpty()) {
        	UserDetails user = userAccountService.buscarPorToken(token);
        	
        	if(user != null) {
	        	UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationTokenBuilder()
	        			.principal(user)
	        			.credentials(null)
	        			.authorities(user.getAuthorities())
	        			.build();
	        	authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(req));
	        	
	            SecurityContextHolder.getContext().setAuthentication(authentication);   	
        	}
        	
    	}
        chain.doFilter(req, res);
    }
}