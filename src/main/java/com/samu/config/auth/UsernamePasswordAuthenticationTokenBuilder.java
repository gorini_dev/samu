package com.samu.config.auth;

import java.util.Collection;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;

public class UsernamePasswordAuthenticationTokenBuilder {

	private Object principal;
	private Object credentials;
	private Collection<? extends GrantedAuthority> authorities;
	
	public UsernamePasswordAuthenticationTokenBuilder principal(Object principal) {
		this.principal = principal;
		return this;
	}
	
	public UsernamePasswordAuthenticationTokenBuilder credentials(Object credentials) {
		this.credentials = credentials;
		return this;
	}	
	
	public UsernamePasswordAuthenticationTokenBuilder authorities(Collection<? extends GrantedAuthority> authorities) {
		this.authorities = authorities;
		return this;
	}	
	
	public UsernamePasswordAuthenticationToken build() {
		return new UsernamePasswordAuthenticationToken(principal, credentials, authorities);
	}
	
}
