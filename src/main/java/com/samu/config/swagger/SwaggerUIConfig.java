package com.samu.config.swagger;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.core.annotation.AuthenticationPrincipal;

import com.samu.SamuApplication;

import springfox.documentation.builders.ParameterBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.service.Parameter;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Configuration
@EnableSwagger2
public class SwaggerUIConfig {
   
	@Bean
    public Docket main() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.basePackage(SamuApplication.class.getPackage().getName()))
                .paths(PathSelectors.any())
                .build()
                .apiInfo(apiInfo())
                .ignoredParameterTypes(AuthenticationPrincipal.class)
                .globalOperationParameters(globalOperationParameters());
    }
        
	private List<Parameter> globalOperationParameters(){
        ParameterBuilder aParameterBuilder = new ParameterBuilder();
        aParameterBuilder.name("Token").modelRef(new ModelRef("string")).parameterType("header").required(false).build();
        List<Parameter> aParameters = new ArrayList<>();
        aParameters.add(aParameterBuilder.build());
        return aParameters;
	}
    
    private ApiInfo apiInfo() {
        return new ApiInfo(
                "SAMU REST API",
                "It contains all needed information to interact with this API.",
                "v1.0",
                "",
                new Contact("SAMU Team", "www.google.com", "gorini.dev@gmail.com"),
                "License of API not yet defined", "", Collections.emptyList());
    }

}
