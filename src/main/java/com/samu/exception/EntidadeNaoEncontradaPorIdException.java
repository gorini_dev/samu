package com.samu.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class EntidadeNaoEncontradaPorIdException extends RuntimeException {
	
	private static final long serialVersionUID = 1L;
	
	private final String id;
		
}
