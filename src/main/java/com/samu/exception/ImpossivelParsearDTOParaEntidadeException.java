package com.samu.exception;

public class ImpossivelParsearDTOParaEntidadeException extends RuntimeException {

	private static final long serialVersionUID = 1L;
	
	public ImpossivelParsearDTOParaEntidadeException(Exception e) {
		super(e);
	}
	
}
