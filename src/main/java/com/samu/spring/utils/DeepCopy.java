package com.samu.spring.utils;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

@Service
public class DeepCopy {
	
	public <T> void copyFields(T from, T to, String... ignoredFields) {
		List<String> ignoredFieldsList = new ArrayList<>();
		for (String value : ignoredFields) {
			ignoredFieldsList.add(value);
		}
		for (Field f : from.getClass().getDeclaredFields()) {
			f.setAccessible(true);
			try {
				if (BeanUtils.isSimpleValueType(f.getType())) {
					if(!ignoredFieldsList.stream().anyMatch(x -> (x).equals(f.getName()))) {
						Field f2 = to.getClass().getDeclaredField(f.getName());
						f2.setAccessible(true);
						f2.set(to,f2.get(from));
					}
				} else {				
					copyFields(f.get(from), f.get(to), ignoredFields);
				}
			} catch (IllegalArgumentException | IllegalAccessException | NoSuchFieldException | SecurityException e) {

			} 
		}
	}
	
}
